# Convolutional Neural Network Programs

These are python programs that use CNN's to create models around image recognition. Originally, they were Python notebooks provided by our professor, Ron Zacharski, giving us an issue and guidelines for our solution.

They have since been adapted to normal python files, and though the comments might be a bit wonky, they should run the same.

## ConvNet Dogs / Cats CNN

This builds a model of pooling and Conv2D layers on thousands of images of dogs and cats. 

We then assess how accurate our model is at determining whether a given JPG is of a dog or cat, and adjust it by using data augmentation and different forms of layers to see which is the best fit. 

## Smile / Frown CNN

Similar to the CNN of Dogs and Cats, this version is adapted to recognize human smiling / frowning in a very similar manner. 

Fair warning, each epoch takes around 5-6 minutes to generate, so I reduced the amount used to 15/20.

## Ending Remarks

Both of these programs revolved around machine image recognition using CNNs. By looking at our accuracy graphs you should be able to see just how accurate a machine can be.