
# coding: utf-8

# In[12]:


import os, shutil

#The New Smile/Frown directory
#os.list_dir
base_dir = '/Users/vincentsummerford/Desktop/smilefrown'
os.mkdir(base_dir)

original_smiles = '/Users/vincentsummerford/Desktop/SMILEs/positives/positives7'
original_frowns = '/Users/vincentsummerford/Desktop/SMILEs/negatives/negatives7'


# Setting up our 3 directories
train_dir = os.path.join(base_dir, 'train')
os.mkdir(train_dir)
validation_dir = os.path.join(base_dir, 'validation')
os.mkdir(validation_dir)
test_dir = os.path.join(base_dir, 'test')
os.mkdir(test_dir)

# Splitting into 3 directories
train_smiles = os.path.join(train_dir, 'smiles')
os.mkdir(train_smiles)

train_frowns = os.path.join(train_dir, 'frowns')
os.mkdir(train_frowns)

validation_smiles = os.path.join(validation_dir, 'smiles')
os.mkdir(validation_smiles)

validation_frowns = os.path.join(validation_dir, 'frowns')
os.mkdir(validation_frowns)

test_smiles = os.path.join(test_dir, 'smiles')
os.mkdir(test_smiles)

test_frowns = os.path.join(test_dir, 'frowns')
os.mkdir(test_frowns)

# Copy first 1000 smiles to trian
fnames_origin2 = os.listdir(original_smiles)
fnames = fnames_origin2[0:1000]
for fname in fnames:
    src = os.path.join(original_smiles, fname)
    dst = os.path.join(train_smiles, fname)
    shutil.copyfile(src, dst)

# Copy next 1000 smiles to valid
fnames = fnames_origin2[1000:2000]
for fname in fnames:
    src = os.path.join(original_smiles, fname)
    dst = os.path.join(validation_smiles, fname)
    shutil.copyfile(src, dst)
    
# Copy next 1000 smiles to to test
fnames = fnames_origin2[2000:3000]
for fname in fnames:
    src = os.path.join(original_smiles, fname)
    dst = os.path.join(test_smiles, fname)
    shutil.copyfile(src, dst)

fnames_origin = os.listdir(original_frowns)

# Copy first 1000 frowns to train
fnames = fnames_origin[:1000]
for fname in fnames:
    src = os.path.join(original_frowns, fname)
    dst = os.path.join(train_frowns, fname)
    shutil.copyfile(src, dst)
    
# Copy next 1000 frowns to validation
fnames = fnames_origin[1000:2000]
for fname in fnames:
    src = os.path.join(original_frowns, fname)
    dst = os.path.join(validation_frowns, fname)
    shutil.copyfile(src, dst)
    
# Copy next 1000 frowns to test
fnames = fnames_origin[2000:3000]
for fname in fnames:
    src = os.path.join(original_frowns, fname)
    dst = os.path.join(test_frowns, fname)
    shutil.copyfile(src, dst)


# In[13]:


#Checking our directories
import os
base_dir = '/Users/vincentsummerford/Desktop/smilefrown'

#Setting up the Training/Validation/Test directories similarly to CovDogsnCats
train_dir = os.path.join(base_dir, 'train')
train_smiles = os.path.join(train_dir, 'smiles')
train_frowns = os.path.join(train_dir, 'frowns')
validation_dir = os.path.join(base_dir, 'validation')
validation_smiles = os.path.join(validation_dir, 'smiles')
validation_frowns = os.path.join(validation_dir, 'frowns')
test_dir = os.path.join(base_dir, 'test')
test_smiles = os.path.join(test_dir, 'smiles')
test_frowns = os.path.join(test_dir, 'frowns')

#Printing out the size of each directory to error check
print('total training smile images:', len(os.listdir(train_smiles)))
print('total training frown images:', len(os.listdir(train_frowns)))
print('total validation smile images:', len(os.listdir(validation_smiles)))
print('total validation frown images:', len(os.listdir(validation_frowns)))
print('total test smile images:', len(os.listdir(test_smiles)))
print('total test frown images:', len(os.listdir(test_frowns)))


# In[14]:


from keras import layers
from keras import models

#Establishing our Conv / Pooling Layers
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(64, 64, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))

model.add(layers.Flatten())
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))

model.summary()


# In[15]:


#Compiling Model
model.compile(optimizer='rmsprop',
                loss='binary_crossentropy',
                metrics=['accuracy'])


# In[16]:


from keras.preprocessing.image import ImageDataGenerator

#Rescaling images
train_datagen = ImageDataGenerator(rescale=1./255)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_dir,
        # Resizing to 64 x 64, which should be the default size.
        target_size=(64, 64),
        batch_size=20,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_dir,
        target_size=(64, 64),
        batch_size=20,
        class_mode='binary')


# In[19]:


history = model.fit_generator(
          train_generator,
          steps_per_epoch=100,
          epochs=30,
          validation_data=validation_generator,
          validation_steps=50)


# In[18]:


import matplotlib.pyplot as plt

acc = history.history['acc']
loss = history.history['loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Loss')
# b is for "solid blue line"
plt.plot(epochs, acc, 'b', label='Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.title('Loss and Accuracy')
plt.legend()
plt.show()

